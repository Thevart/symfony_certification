<?php

namespace Certification\FormBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Unicorn
{
    const COLOR_PINK = 'pink';
    const COLOR_RAINBOW = 'rainbow';
    const COLOR_BLUE = 'blue';

    public static function getValidColors()
    {
        return [
            self::COLOR_PINK,
            self::COLOR_RAINBOW
        ];
    }

    protected $name;

    protected $color;

    protected $favoriteCandy;

    protected $dateOfUnicornisation;

    /**
     * @Assert\Length(
     *      min = 2,
     *      max = 100,
     *      minMessage = "The horn of a unicorn cannot be so small",
     *      maxMessage = "The horn of a unicorn cannot be so big"
     * )
     * @Assert\Type("float")
     * @var float
     */
    protected $hornLength;

    protected $title;

    protected $amountOfCandyPerHour;

    protected $orginalHorse;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     *
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFavoriteCandy()
    {
        return $this->favoriteCandy;
    }

    /**
     * @param mixed $favoriteCandy
     *
     * @return $this
     */
    public function setFavoriteCandy($favoriteCandy)
    {
        $this->favoriteCandy = $favoriteCandy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateOfUnicornisation()
    {
        return $this->dateOfUnicornisation;
    }

    /**
     * @param mixed $dateOfUnicornisation
     *
     * @return $this
     */
    public function setDateOfUnicornisation($dateOfUnicornisation)
    {
        $this->dateOfUnicornisation = $dateOfUnicornisation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHornLength()
    {
        return $this->hornLength;
    }

    /**
     * @param mixed $hornLength
     *
     * @return $this
     */
    public function setHornLength($hornLength)
    {
        $this->hornLength = $hornLength;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmountOfCandyPerHour()
    {
        return $this->amountOfCandyPerHour;
    }

    /**
     * @param mixed $amountOfCandyPerHour
     *
     * @return $this
     */
    public function setAmountOfCandyPerHour($amountOfCandyPerHour)
    {
        $this->amountOfCandyPerHour = $amountOfCandyPerHour;

        return $this;
    }
}