<?php

namespace Certification\FormBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CertificationFormBundle:Default:index.html.twig');
    }
}
