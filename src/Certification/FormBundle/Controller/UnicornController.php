<?php

namespace Certification\FormBundle\Controller;

use Certification\FormBundle\Entity\Unicorn;
use Certification\FormBundle\Type\UnicornCreateType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

class UnicornController extends Controller
{
    public function successPinkAction()
    {
        return $this->render('CertificationFormBundle:Unicorn:successPink.html.twig', array());
    }

    public function successRainbowAction()
    {
        return $this->render('CertificationFormBundle:Unicorn:successRainbow.html.twig', array());
    }

    public function createBasicAction(Request $request)
    {
        $unicorn = new Unicorn();

        /** @var FormBuilder $builder */
        $builder = $this->createFormBuilder($unicorn)
            ->add('name', TextType::class)
            ->add('color', ChoiceType::class,
                array(
                    'choices' => array (
                        'Pink' => Unicorn::COLOR_PINK,
                        'Rainbow' => Unicorn::COLOR_RAINBOW
                    )))
            ->add('hornLength', NumberType::class)
            ->add('save', SubmitType::class, array('label' => 'Create Unicorn'));

        $builder->get('hornLength')
            ->addModelTransformer(new CallbackTransformer(
                function ($lenthInInch) {
                    return $lenthInInch * 2.34;
                },
                function ($lenthInCm) {
                    return $lenthInCm * 0.3937;
                }
            ))
        ;

        /** @var Form $form */
        $form = $builder->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $unicorn = $form->getData();

            if($unicorn->getColor() == Unicorn::COLOR_RAINBOW){
                return $this->redirectToRoute('certification_form_unicorn_create_success_rainbow');
            }else{
                return $this->redirectToRoute('certification_form_unicorn_create_success_pink');
            }
        }

        return $this->render('CertificationFormBundle:Unicorn:new.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    public function createAdvanceAction(Request $request)
    {
        $unicorn = new Unicorn();
        $unicorn->setHornLength(10);
        $form = $this->createForm(UnicornCreateType::class, $unicorn);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $unicorn = $form->getData();

            if($unicorn->getColor() == Unicorn::COLOR_RAINBOW){
                return $this->redirectToRoute('certification_form_unicorn_create_success_rainbow');
            }else{
                return $this->redirectToRoute('certification_form_unicorn_create_success_pink');
            }
        }

        return $this->render('CertificationFormBundle:Unicorn:new_advanced.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}