<?php

namespace Certification\FormBundle\Type\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;

class LengthTransformer implements DataTransformerInterface
{

    const INCH_TO_CM = 2.34;
    const CM_TO_INCH = 0.3937;

    public function transform($lengthInInch)
    {
        return $lengthInInch * self::INCH_TO_CM;
    }

    public function reverseTransform($lengthInCM)
    {
        return $lengthInCM * self::CM_TO_INCH;
    }
}