<?php

namespace Certification\FormBundle\Type;

use Certification\FormBundle\Entity\Unicorn;
use Certification\FormBundle\Type\DataTransformer\LengthTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UnicornCreateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'empty_data' => 'Default name',
                'label' => 'Name of your Unicorn',
                'label_attr' => [
                    'class' => 'unicorn_form_pink',
                    'style' => 'color:pink'
                ],
                'required' => true,
                'trim' => true
            ])
            ->add('color', ChoiceType::class, [
                'multiple' => false,
                'expanded' => true,
                'placeholder' => 'Choose a color for you Unicorn',
                'preferred_choices' => array('pink'),
                'choices'  => array(
                    'Unicolor' => [
                        'Pink' => Unicorn::COLOR_PINK,
                    ],
                    'Multicolor' => [
                        'Rainbow' => Unicorn::COLOR_RAINBOW
                    ]
                ),
                'choice_attr' => function($val, $key, $index) {
                    // adds a class like attending_yes, attending_no, etc
                    return ['class' => 'attending_'.strtolower($key), 'style' => 'color:'.$val];
                },
            ])
            ->add('dateOfUnicornisation', DateType::class, [
                'widget' => 'single_text'
            ])
            ->add('hornLength', NumberType::class)
            ->add('title', ChoiceType::class, [
                'choices' => [
                    'Male' => 'male',
                    'Female' => 'female'
                ]
            ])
            ->add('unicornCreationAgreement', CheckboxType::class, [
                'label' => 'Do you agree to give to the Administrator of this company the rights to use this Unicorn ?',
                'mapped' => false
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Submit your Unicorn'
            ])
        ;

        $builder->get('hornLength')
            ->addModelTransformer(new LengthTransformer());

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Unicorn::class,
            'csrf_protection' => true,
            'csrf_field_name' => '_token',
            // a unique key to help generate the secret token
            'csrf_token_id'   => 'unicorn_item',
        ));
    }



}